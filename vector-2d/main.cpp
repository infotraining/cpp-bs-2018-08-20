#include <cmath>
#include <iostream>
#include <string>
#include <iomanip>
#include <algorithm>

#include "catch.hpp"

using namespace std;

namespace Vectors
{
    class Vector2D
    {
        double x_ = 0.0;
        double y_ = 0.0;

    public:
        Vector2D() = default;

        Vector2D(double x, double y)
            : x_{x}
            , y_{y}
        {
        }

        double x() const; // explicit inline defined outside class

        double y() const // implicit inline
        {
            return y_;
        }

        double length() const
        {
            return sqrt(pow(x_, 2.0) + pow(y_, 2.0));
        }

        bool operator==(Vector2D const& other) const
        {
            return x() == other.x() && y() == other.y();
        }

        bool operator!=(Vector2D const& other) const
        {
            return !(*this == other);
        }

        Vector2D operator+(Vector2D const& other) const
        {
            return {x() + other.x(), y() + other.y()};
        }

        Vector2D operator-(Vector2D const& other) const
        {
            return {x() - other.x(), y() - other.y()};
        }

        Vector2D operator-() const
        {
            return {-x(), -y()};
        }

        double operator*(Vector2D const& other) const
        {
            return x() * other.x() + y() * other.y();
        }

        bool operator<(Vector2D const& other) const
        {
            return length() < other.length();
        }

        static Vector2D versor_x()
        {
            return Vector2D{1.0, 0.0};
        }

        static Vector2D versor_y()
        {
            return {0.0, 1.0};
        }
    };

    Vector2D operator*(double a, Vector2D const& vec)
    {
        return {a * vec.x(), a * vec.y()};
    }

    Vector2D operator*(Vector2D const& vec, double a)
    {
        return a * vec;
    }

    ostream& operator<<(ostream& out, Vector2D const& vec)
    {
        out << "Vector2D("
            << showpoint << setprecision(2) << vec.x() << ", " << vec.y() << ")";

        return out;
    }

    //    Vector2D operator+(Vector2D const& a, Vector2D const& b)
    //    {
    //        return {a.x() + b.x(), a.y() + b.y()};
    //    }

    void print(Vector2D const& vec)
    {
        cout << "Vector2D(" << vec.x() << ", " << vec.y() << ")" << endl;
    }

    inline double Vector2D::x() const
    {
        return x_;
    }
}

using namespace Vectors;

TEST_CASE("vector2D")
{
    SECTION("default constructor")
    {
        Vector2D vec;

        REQUIRE(vec.x() == Approx(0.0));
        REQUIRE(vec.y() == Approx(0.0));
    }

    SECTION("constructor")
    {
        const Vector2D vec{1.0, 2.0};

        REQUIRE(vec.x() == Approx(1.0));
        REQUIRE(vec.y() == Approx(2.0));
    }

    SECTION("length")
    {
        Vector2D vec{1.0, 1.0};

        REQUIRE(vec.length() == Approx(1.41).margin(0.01));
    }

    SECTION("versor x")
    {
        Vector2D vx = Vector2D::versor_x();

        REQUIRE(vx.x() == Approx(1.0));
        REQUIRE(vx.y() == Approx(0.0));
    }

    SECTION("versor y")
    {
        Vector2D vy = Vector2D::versor_y();

        REQUIRE(vy.x() == Approx(0.0));
        REQUIRE(vy.y() == Approx(1.0));
    }

    SECTION("operators")
    {

        const Vector2D vec1{1.0, 2.0};
        const Vector2D vec2{2.0, 0.5};

        SECTION("operator +")
        {
            auto vec = vec1 + vec2;

            REQUIRE(vec.x() == 3.0);
            REQUIRE(vec.y() == 2.5);
        }

        SECTION("equality")
        {
            REQUIRE(vec1 == Vector2D{1.0, 2.0});
        }

        SECTION("nonequality")
        {
            REQUIRE(vec1 != vec2);
        }

        SECTION("subtraction")
        {
            auto vec = vec1 - vec2;

            REQUIRE(vec == Vector2D{-1.0, 1.5});
        }

        SECTION("negate")
        {
            auto vec = -vec1;

            REQUIRE(vec == Vector2D{-1.0, -2.0});
        }

        SECTION("multiplication")
        {
            double scalar = vec1 * vec2;

            REQUIRE(scalar == Approx(3.0));
        }

        SECTION("multiplication by double")
        {
            SECTION("a * Vector2D")
            {
                Vector2D vec = 2.0 * vec1;

                REQUIRE(vec == Vector2D{2.0, 4.0});
            }

            SECTION("Vector2D * a")
            {
                Vector2D vec = vec1 * 2.0;

                REQUIRE(vec == Vector2D{2.0, 4.0});
            }
        }

        SECTION("operator <<")
        {
            cout << "vec1: " << vec1 << endl;

            stringstream ss;

            ss << vec1;

            REQUIRE(ss.str() == "Vector2D(1.0, 2.0)");
        }
    }
}

bool compare_by_length_desc(Vector2D const& a, Vector2D const& b)
{
    return a.length() > b.length();
}

struct CompareByLengthDesc
{
    bool operator()(Vector2D const& a, Vector2D const& b)
    {
        return a.length() > b.length();
    }
};

TEST_CASE("sorting using length")
{
    vector<Vector2D> vecs = { {1.0, 2.0 }, {3.0, 2.0}, {0.0, 1.0}, {7.0, 0.0} };

    for(const auto& v : vecs)
        cout << v << " ";
    cout << endl;

    sort(begin(vecs), end(vecs));

    for(const auto& v : vecs)
        cout << v << " ";
    cout << endl;

    SECTION("descending by length")
    {
        //sort(begin(vecs), end(vecs), compare_by_length_desc); // deprecated using of function
        sort(begin(vecs), end(vecs), CompareByLengthDesc{});

        for(const auto& v : vecs)
            cout << v << " ";
        cout << endl;
    }

    SECTION("count vecs with length more than avg")
    {
        double length_sum{};

        for_each(begin(vecs), end(vecs), [&](Vector2D const& v) { length_sum += v.length(); });

        double length_threshold = length_sum / vecs.size();

        size_t result = count_if(begin(vecs), end(vecs),
                                 [=](Vector2D const& v) { return v.length() > length_threshold; });

        REQUIRE(result == 2);
    }
}
