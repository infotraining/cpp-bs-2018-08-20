#include <string>
#include <iostream>

using namespace std;

enum DayOfWeek : uint8_t { mon = 1, tue, wed, thd, fri, sat, sun };

// since C++11
enum class Coffee : uint8_t { espresso = 1, latte, cappucino, chemex };

int main()
{
    DayOfWeek day = mon;
    day = static_cast<DayOfWeek>(4);
    cout << "day: " << day << endl;
    int value = mon;
    cout << "day value: " << value << endl;
    cout << sizeof(DayOfWeek) << endl;

    // scoped enum
    Coffee coffee = Coffee::espresso;
    coffee = static_cast<Coffee>(3);
    cout << "c: " << static_cast<int>(coffee) << endl;
    int coffee_value = static_cast<int>(coffee);
    cout << "coffee_value: " << coffee_value << endl;
    cout << sizeof(Coffee) << endl;
}
