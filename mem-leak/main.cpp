#include <string>
#include <iostream>
#include <memory>

using namespace std;

int main()
{
    string* heap_str = new string("text");

    cout << *heap_str << endl;

    delete heap_str;

    // cout << *heap_str << endl;

    auto heap_int = make_unique<int>(10);

    cout << *heap_int << endl;
}
