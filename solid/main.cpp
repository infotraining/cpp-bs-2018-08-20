#include <algorithm>
#include <fstream>
#include <iostream>
#include <iterator>
#include <list>
#include <numeric>
#include <stdexcept>
#include <string>
#include <vector>
#include <memory>
#include <cmath>
#include <unordered_map>

struct StatResult
{
    std::string description;
    double value;

    StatResult(const std::string& desc, double val) : description(desc), value(val)
    {
    }
};

using Data = std::vector<double>;
using Results = std::vector<StatResult>;

//enum StatisticsType
//{
//    avg,
//    min_max,
//    sum
//};

class Statistics
{
public:
    virtual ~Statistics() = default;
    virtual Results calculate(Data const& data) = 0;
};

class Avg : public Statistics
{
public:
    Results calculate(const Data& data) override
    {
        double sum = std::accumulate(data.begin(), data.end(), 0.0);
        double avg = sum / data.size();

        return { {"AVG", avg } };
    }
};

class StdDev : public Statistics
{
public:
    Results calculate(const Data& data) override
    {
        Avg avg;

        double mean = avg.calculate(data).front().value;

        double variance = {};

        for(const auto& item : data)
        {
            variance += std::pow(item - mean, 2);
        }

        double std_dev = std::sqrt(variance / (data.size() - 1));

        return { {"STD_DEV", std_dev } };
    }
};

class MinMax : public Statistics
{
public:
    Results calculate(const Data& data) override
    {
        double min = *(std::min_element(data.begin(), data.end()));
        double max = *(std::max_element(data.begin(), data.end()));

        Results results;
        results.push_back(StatResult("MIN", min));
        results.push_back(StatResult("MAX", max));

        return results;
    }
};

class Sum : public Statistics
{
public:
    Results calculate(const Data& data) override
    {
        double sum = std::accumulate(data.begin(), data.end(), 0.0);

        return { {"SUM", sum} };
    }
};

using StatisticsPtr = std::shared_ptr<Statistics>;

class StatGroup : public Statistics
{
    std::vector<StatisticsPtr> stats_;
public:
    Results calculate(const Data& data) override
    {
        Results stat_results;

        for(auto const& stat : stats_)
        {
            auto temp = stat->calculate(data);

            stat_results.insert(end(stat_results), begin(temp), end(temp));
        }

        return stat_results;
    }

    void add(StatisticsPtr stat)
    {
        stats_.push_back(stat);
    }
};

class DataParser
{
public:
    virtual ~DataParser() = default;
    virtual Data parse_file(std::string const& file_name) = 0;
};

class DatDataParser : public DataParser
{
public:
    Data parse_file(const std::string& file_name) override
    {
        Data data;

        std::ifstream fin(file_name.c_str());
        if (!fin)
            throw std::runtime_error("File not opened");

        double d;
        while (fin >> d)
        {
            data.push_back(d);
        }

        return data;
    }
};

class DataAnalyzer
{
    StatisticsPtr stat_;
    Data data_;
    Results results_;

    static std::unordered_map<std::string, std::unique_ptr<DataParser>> parsers_;

    std::string extract_extension(std::string const& file_name)
    {
        auto dot_pos = find(file_name.rbegin(), file_name.rend(), '.');

        return std::string(dot_pos.base(), end(file_name));
    }

public:
    DataAnalyzer(StatisticsPtr stat) : stat_{stat}
    {
    }

    void load_data(const std::string& file_name)
    {
        data_.clear();
        results_.clear();

        std::string extension = extract_extension(file_name);

        data_ = parsers_.at(extension)->parse_file(file_name);

        std::cout << "File " << file_name << " has been loaded...\n";
    }

    void set_statistics(StatisticsPtr stat)
    {
        stat_ = stat;
    }

    void calculate()
    {
        auto stat_results = stat_->calculate(data_);

        results_.insert(end(results_), begin(stat_results), end(stat_results));
    }

    const Results& results() const
    {
        return results_;
    }

    static void register_parser(std::string const& name, std::unique_ptr<DataParser> parser)
    {
        parsers_.insert(std::make_pair(name, std::move(parser)));
    }
};

std::unordered_map<std::string, std::unique_ptr<DataParser>> DataAnalyzer::parsers_;

void show_results(const Results& results)
{
    for (const auto& rslt : results)
        std::cout << rslt.description << " = " << rslt.value << std::endl;
}

int main()
{
    DataAnalyzer::register_parser("dat", std::make_unique<DatDataParser>());

    auto avg = std::make_shared<Avg>();
    auto min_max = std::make_shared<MinMax>();
    auto sum = std::make_shared<Sum>();
    auto std_dev = std::make_shared<StdDev>();

    auto std_stats = std::make_shared<StatGroup>();
    std_stats->add(avg);
    std_stats->add(min_max);
    std_stats->add(sum);
    std_stats->add(std_dev);

    DataAnalyzer da{std_stats};
    da.load_data("data.dat");
    da.calculate();    

    show_results(da.results());

    std::cout << "\n\n";

    da.load_data("new_data.dat");
    da.calculate();       

    show_results(da.results());
}
