#include <algorithm>
#include <iostream>
#include <string>

#include "catch.hpp"

using namespace std;

class Array
{
    size_t size_;
    int* items_;

public:
    using iterator = int*;
    using const_iterator = int const*;

    Array(size_t size)
        : size_{size}
        , items_{new int[size]}
    {
        cout << "Creating array: " << size_ << endl;

        fill_n(items_, size_, 0);
    }

    Array(initializer_list<int> lst)
        : size_{lst.size()}
        , items_{new int[size_]}
    {
        copy(lst.begin(), lst.end(), items_);
    }

    // copy constructor
    Array(Array const& source)
        : size_{source.size_}
        , items_{new int[size_]}
    {
        cout << "Copy of array: " << source.size() << endl;
        copy(source.items_, source.items_ + source.size_, items_);
    }

    // copy assignment operator
    Array& operator=(Array const& source)
    {
        if (this != &source)
        {
            // clean-up the old state
            delete[] items_;

            // assignment of a new state
            size_ = source.size_;
            items_ = new int[size_];

            copy(source.items_, source.items_ + source.size_, items_);
        }

        return *this;
    }

    // move constructor
    Array(Array&& source) noexcept
        : size_{source.size_}, items_{source.items_}
    {
        cout << "Move of array: " << source.size() << endl;

        source.size_ = 0;
        source.items_ = nullptr;
    }

    // move assignment operator
    Array& operator=(Array&& source)
    {
        if (this != &source)
        {
            // clean-up the old state
            delete[] items_;

            size_ = source.size_;
            items_ = source.items_;

            source.size_ = 0;
            source.items_ = nullptr;
        }

        return *this;
    }

    ~Array()
    {
        delete[] items_;
    }

    size_t size() const
    {
        return size_;
    }

    int& operator[](size_t index)
    {
        return *(items_ + index);
    }

    int const& operator[](size_t index) const
    {
        return *(items_ + index);
    }

    iterator begin()
    {
        return items_;
    }

    const_iterator begin() const
    {
        return items_;
    }

    iterator end()
    {
        return items_ + size_;
    }

    const_iterator end() const
    {
        return items_ + size_;
    }
};

ostream& operator<<(ostream& out, Array const& tab)
{
    out << "[ ";

//    for (size_t i = 0; i < tab.size(); ++i)
//        out << tab[i] << " ";

    for(const auto& item : tab)
        cout << item << " ";

    out << "]";

    return out;
}

TEST_CASE("Array")
{
    SECTION("works as dynamic array")
    {
        Array tab(10);

        int i = 0;
        for(auto it = begin(tab); it != end(tab); ++it)
            *it = ++i;


        cout << tab << endl;
    }

    SECTION("can be initialized from list")
    {
        Array tab = {1, 2, 3, 4};

        cout << tab << endl;
    }

    SECTION("is copyable")
    {
        Array tab = {1, 2, 3, 4};

        SECTION("copy constructor")
        {
            Array backup = tab; // copy tab to backup

            cout << backup << endl;
        }

        SECTION("copy assignment")
        {
            Array new_tab = {5, 6, 7};

            tab = new_tab;

            cout << tab << endl;
        }
    }
}

Array create_large_array()
{
    Array a(1'000'000);

    for(size_t i = 0; i < a.size(); ++i)
        a[i] = i;

    return a;
}

TEST_CASE("large array - move semantics")
{
    Array big_array = create_large_array();

    REQUIRE(big_array.size() == 1'000'000);

    vector<Array> matrix;

    matrix.push_back(create_large_array());
    matrix.push_back(create_large_array());
    matrix.push_back(create_large_array());

    Array row(1'000'000);

    matrix.push_back(move(row));

    row = create_large_array();

}

TEST_CASE("vector and constructors")
{
    vector<int> vec1(10);
    REQUIRE(vec1.size() == 10);

    vector<int> vec2{10};
    REQUIRE(vec2.size() == 1);
    REQUIRE(vec2[0] == 10);
}

string full_name(string const& fn, string const& ln)
{
    return fn + " " + ln;
}

TEST_CASE("ref binding rules")
{
    SECTION("C++98")
    {
        SECTION("l-value can be bound to l-value ref")
        {
            int x = 10;
            int& ref_x = x;
        }

        SECTION("r-value can be bound to l-value const ref")
        {
            string const& lref_str = full_name("Jan", "Kowalski");
        }
    }

    SECTION("C++11")
    {
        SECTION("r-value can be bound to r-value ref")
        {
            string&& rref_str = full_name("Jan", "Kowalski");

            rref_str[0] = 'P';
        }

        SECTION("l-value cannot be bound to r-value ref")
        {
            int x = 10;
            //int&& rref_x = x;  // ERROR
        }
    }
}


struct Data
{
    Array data;

    Data(size_t size)
        : data(size)
    {}

//    Data(const Data&) = default;
//    Data(Data&&) = default;
//    Data& operator=(const Data&) = default;
//    Data& operator=(Data&&) = default;

//    ~Data()
//    {

//    }
};

TEST_CASE("Data")
{
    cout << "\n\n";

    vector<Data> vec;

    vec.push_back(Data(2'000'000));
    vec.push_back(Data(2'000'000));
    vec.push_back(Data(2'000'000));
    vec.push_back(Data(2'000'000));
}
