#define CATCH_CONFIG_MAIN

#include "bank_account.hpp"
#include "catch.hpp"

using namespace std;

TEST_CASE("creating bank account", "[constructors]")
{
    using namespace Bank;

    SECTION("with id & name")
    {
        BankAccount ba{665, "Jan Kowalski"};

        REQUIRE(ba.id() == 665);
        REQUIRE(ba.owner() == "Jan Kowalski");
        REQUIRE(ba.balance() == Approx(0.0));
    }

    SECTION("with id & name & balance")
    {
        BankAccount ba{665, "Jan Kowalski", 1000.0};

        REQUIRE(ba.id() == 665);
        REQUIRE(ba.owner() == "Jan Kowalski");
        REQUIRE(ba.balance() == Approx(1000.0));
    }
}

TEST_CASE("deposit money", "[deposit]")
{
    using namespace Bank;

    // Arrange
    BankAccount ba{665, "Jan Kowalski"};

    // Act
    ba.deposit(500.0);

    // Assert
    REQUIRE(ba.balance() == Approx(500.0));
}

SCENARIO("withdraw")
{
    using namespace Bank;

    GIVEN("I have account")
    {
        BankAccount ba{665, "Jan Kowalski", 1000.0};

        WHEN("I make a withdraw")
        {
            ba.withdraw(500.0);

            THEN("amount is subtracted from balance")
            {
                REQUIRE(ba.balance() == Approx(500.0));
            }
        }

        WHEN("I make a withdraw with amount higher balance")
        {
            THEN("exception is thrown")
            {
                REQUIRE_THROWS_AS(ba.withdraw(1500.0), InsufficientFunds);
            }

            THEN("exception describes an error")
            {
                try
                {
                    ba.withdraw(1500.0);
                }
                catch (InsufficientFunds const& e)
                {
                    REQUIRE(e.id == 665);
                    REQUIRE(e.balance == Approx(1000.0));
                    REQUIRE(e.amount == Approx(1500.0));
                }
            }
        }
    }
}

TEST_CASE("interest rate", "[interest]")
{
    using namespace Bank;

    SECTION("default interest rate")
    {
        REQUIRE(BankAccount::interest_rate() == 0.1);
    }

    SECTION("changing interest rate")
    {
        BankAccount::set_interest_rate(0.05);

        REQUIRE(BankAccount::interest_rate() == Approx(0.05));
    }
}

TEST_CASE("paying interest rate", "[interest]")
{
    using namespace Bank;

    // Arrange
    BankAccount ba{665, "Jan Kowalski", 1000.0};
    BankAccount::set_interest_rate(0.2);

    // Act
    ba.pay_interest();

    // Assert
    REQUIRE(ba.balance() == Approx(1200.0));
}
