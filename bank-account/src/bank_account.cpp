#include "bank_account.hpp"
#include <iostream>
#include <string>

using namespace std;

double Bank::BankAccount::interest_rate_ = 0.1;

Bank::BankAccount::BankAccount(int id, const std::string& owner, double balance)
    : id_{id}
    , owner_{owner}
    , balance_{balance}
{
}
