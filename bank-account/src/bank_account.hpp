#ifndef BANK_ACCOUNT_HPP
#define BANK_ACCOUNT_HPP

#include <string>
#include <cassert>

namespace Bank
{
    class InsufficientFunds : public std::exception
    {
    public:
        const int id;
        const double balance;
        const double amount;

         InsufficientFunds(int id, double balance, double amount)
             : id{id}, balance{balance}, amount{amount}
         {}

         const char* what() const noexcept override
         {
             return "Not enough money!";
         }
    };

    class BankAccount
    {
        const int id_;
        std::string owner_;
        double balance_ = 0.0;        
        static double interest_rate_; // declaration

    public:                        
        BankAccount(int id, std::string const& owner, double balance = 0.0);

        int id() const
        {
            return id_;
        }

        std::string owner() const
        {
            return owner_;
        }

        double balance() const
        {
            return balance_;
        }

        void deposit(double amount)
        {
            assert(amount > 0.0);

            balance_ += amount;
        }

        void withdraw(double amount)
        {
            assert(amount > 0.0);

            if (amount > balance_)
                throw InsufficientFunds{id_, balance_, amount};

            balance_ -= amount;
        }

        void pay_interest()
        {
            balance_ *= 1.0 + interest_rate_;
        }

        static double interest_rate()
        {
            return interest_rate_;
        }

        static void set_interest_rate(double new_interest_rate)
        {
            interest_rate_ = new_interest_rate;
        }
    };
}

#endif
