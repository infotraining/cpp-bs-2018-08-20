#include <iostream>
#include <string>
#include <cmath>

#include "catch.hpp"

using namespace std;

bool is_prime(int x)
{
    bool is_prime_number = true;

    for(int n = 2; n <= static_cast<int>(sqrt(x)); ++n)
        if (x % n == 0)
        {
            is_prime_number = false;
            break;
        }
    return is_prime_number;
}

vector<int> primes(int upper_limit)
{
    vector<int> found_primes;

    for(int n = 2; n <= upper_limit; ++n)
        if (is_prime(n))
            found_primes.push_back(n);

    return found_primes;
}

TEST_CASE("is_prime", "[primes]")
{
    REQUIRE(is_prime(13) == true);
    REQUIRE(is_prime(9) == false);

    REQUIRE_FALSE(is_prime(8));
    REQUIRE(is_prime(17));
}

TEST_CASE("primes", "[primes]")
{
    REQUIRE(primes(10) == vector<int>{ 2, 3, 5, 7 });

    using namespace Catch::Matchers;

    REQUIRE_THAT(primes(20), Equals(vector<int>{2, 3, 5, 7, 11, 13, 17, 19}));
}

TEST_CASE("pushing to vector", "[vec]")
{
    namespace CM = Catch::Matchers; // alias for namespace

    vector<int> vec = { 1, 2, 3 };

    REQUIRE(vec.size() == 3);
    REQUIRE(vec.capacity() == 3);
    REQUIRE_THAT(vec, CM::Equals(vector<int> {1, 2, 3}));

    SECTION("pushing one item")
    {
        vec.push_back(4);

        REQUIRE(vec.size() == 4);
        REQUIRE(vec.capacity() >= 4);
        REQUIRE_THAT(vec, CM::Equals(vector<int>{1, 2, 3, 4}));
    }
    SECTION("pushing many items")
    {
        vec.insert(vec.end(), { 4, 5, 6});

        REQUIRE(vec.size() == 6);
        REQUIRE(vec.capacity() >= 6);
        REQUIRE_THAT(vec, CM::Equals(vector<int>{1, 2, 3, 4, 5, 6}));
    }
}
