#define CATCH_CONFIG_MAIN

#include "catch.hpp"
#include "sum.hpp"

using namespace std;

TEST_CASE("full name")
{
    string fn = "Jan";
    string ln = "Kowalski";

    REQUIRE(full_name(fn, ln) == "Jan Kowalski");
}