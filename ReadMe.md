# Dokumentacja

* https://infotraining.bitbucket.io/cpp-bs/

# Linki

* https://github.com/isocpp/CppCoreGuidelines/
* https://www.youtube.com/watch?v=JfmTagWcqoE

# Books

* A Tour of C++, Second Edition - BS
* Discovering Modern C++: An Intensive Course for Scientists, Engineers, and Programmers
