#include <string>
#include <iostream>
#include <vector>
#include <limits>
#include <fstream>

using namespace std;

double minimum(double a, double b)
{
    return (a < b) ? a : b;
}

using Data = vector<double>;  // the same as typedef vector<double> Data;

double minimum(Data const& data)
{
    double min_value = numeric_limits<double>::max();

    for(const auto& item : data)
        if (item < min_value)
            min_value = item;

    return min_value;
}

void init(vector<double>& data, int size, double init = 0.0, double step = 1.0)
{
    double value = init;

    for(int i = 0; i < size; ++i)
    {
        data.push_back(value);
        value += step;
    }
}

vector<int> load_from_file(string const& path)
{
    vector<int> data;

    ifstream file_in{path};

    if (!file_in)
    {
        cout << "Error! File not opened!" << endl;
        exit(13);
    }

    while(true)
    {
        int value;
        file_in >> value;

        if (!file_in)
            break;

        data.push_back(value);
    }

    return data;
}

void increment(int* ptr_x) // ptr_x is in-out parameter (optional - maybe nullptr)
{
    if (ptr_x)
        ++(*ptr_x);
}

void increment(int& x) // x is in-out (requires value)
{
    ++x;
}


template <typename T>
auto multiply(T x, T y)
{
    return x * y;
}

enum class Coffee { espresso, latte, chemex };

auto get_description(Coffee coffee)
{
    switch(coffee)
    {
        case Coffee::espresso:
            return "Espresso"s;
        case Coffee::latte:
            return "Latte"s;
        case Coffee::chemex:
            return "Chemex"s;
        default:
            return "Just coffee"s;
    }
}

int main()
{
    double a = 10;
    double b = 20;

    auto min = minimum(a, b);

    cout << "min: " << min << endl;

    int x = 665;
    increment(&x);

    cout << "x after call: " << x << endl;

    int* ptr = nullptr;
    increment(ptr);

    vector<double> vec = { 3.15, 5.235, 6.345, 0.01 };
    init(vec, 10, -0.5);

    double min_value = minimum(vec);

    cout << "vec: ";
    for(const auto& item : vec)
        cout << item << " ";
    cout << endl;

    cout << "min_value: " << min_value << endl;


    const vector<int> values = load_from_file("data.txt");

    cout << "values: ";
    for(const int& item : values)
        cout << item << "; ";
    cout << endl;

    cout << "4 * 5 = " << multiply(4, 5) << endl;
}
