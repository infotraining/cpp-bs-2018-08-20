#include <iostream>
#include <string>

#include "catch.hpp"

using namespace std;

class Person
{
    string name_;
    int age_ = -1;
public:
    Person() = default;

    Person(string const& name)
        : Person{name, -1} // delegeting constructor - since C++11
    {
        // ...
    }

    Person(string const& name, int age)
        : name_{name}, age_{age}
    {}

    string name() const
    {
        return name_;
    }

    int age() const
    {
        return age_;
    }
};


TEST_CASE("constructors")
{
    Person p;

    REQUIRE(p.name() == ""s);
    REQUIRE(p.age() == -1);
}

class Secret; // forward declaration

struct Stranger
{
    void change_value(Secret& s);
};

class Secret
{
    int value = 665;

    friend void change_value(Secret&); // declaration of frienship with a function
    friend void Stranger::change_value(Secret&);
    friend class Partner;
};

void Stranger::change_value(Secret& s)
{
    s.value = 42;
}

void change_value(Secret& s)
{
    s.value = 13;
}

class Partner
{
public:
    void publish(Secret& s)
    {
        cout <<  s.value << endl;
    }
};


TEST_CASE("friends")
{
    Secret s;
    change_value(s);

    Partner p;
    p.publish(s);

    Stranger other;
    other.change_value(s);
    p.publish(s);
}

