#include <cstring>
#include <iostream>
#include <string>
#include <algorithm>

#include "catch.hpp"

using namespace std;

template <typename T>
T const& max_value(T const& x, T const& y)
{
    return (x < y) ? y : x;
}

const char* max_value(const char* x, const char* y)
{
    return (std::strcmp(x, y) < 0) ? y : x;
}

TEST_CASE("max_value")
{
    REQUIRE(max_value(5, 10) == 10);
    REQUIRE(max_value<double>(3.14, 2) == Approx(3.14));
    REQUIRE(max_value("abc"s, "def"s) == "def"s);
    REQUIRE(max_value("abc", "aaa") == "abc"s);
}

bool is_even(int x)
{
    return x % 2 == 0;
}

template <typename Iterator, typename Predicate>
Iterator my_find_if(Iterator start, Iterator end, Predicate pred)
{
    for(Iterator it = start; it != end; ++it)
        if (pred(*it))
            return it;

    return end;
}

template <typename Container>
void print(Container const& c, string const& prefix)
{
    cout << prefix << ": [ ";
    for(auto const& item : c)
        cout << item << " ";
    cout << "]\n";
}

TEST_CASE("my_find_if")
{
    vector<int> vec = {665, 43, 1, 7, 42, 99, 101};

    print(vec, "vec");

    SECTION("with function pointer")
    {
        auto pos = my_find_if(begin(vec), end(vec), &is_even);

        if (pos != end(vec))
            cout << "Found " << *pos << endl;
    }

    SECTION("with lambda")
    {
        auto pos = my_find_if(begin(vec), end(vec), [](int x) { return x % 2 == 0; });

        if (pos != end(vec))
            cout << "Found " << *pos << endl;
    }
}

struct Integer
{
    int value;

    explicit Integer(int v) : value{v}
    {}
};

void foo(Integer i)
{
    cout << "foo(" << i.value << ")\n";
}

TEST_CASE("implicit conversion with constructor")
{
    Integer i1(10);
    Integer i2{20};
    //Integer i3 = 5;
    //foo(13);
    foo(Integer{13});
}


template <typename T>
class UniquePtr
{
    T* ptr_ = nullptr;
public:
    UniquePtr() = default;

    explicit UniquePtr(T* ptr) : ptr_{ptr}
    {}

    ~UniquePtr()
    {
        delete ptr_;
    }

    UniquePtr(UniquePtr const& other) = delete;
    UniquePtr& operator=(UniquePtr const& other) = delete;

    UniquePtr(UniquePtr&& other) noexcept
        : ptr_{other.ptr_}
    {
        other.ptr_ = nullptr;
    }

    UniquePtr& operator=(UniquePtr& other) noexcept
    {
        if (this != &other)
        {
            delete ptr_;

            ptr_ = other.ptr_;
            other.ptr_ = nullptr;
        }

        return *this;
    }

    T& operator*() const
    {
        return *ptr_;
    }

    T* operator->() const
    {
        return ptr_;
    }

    explicit operator bool() const
    {
        return ptr_ != nullptr;
    }

    T* get() const
    {
        return ptr_;
    }
};

void use(UniquePtr<int> ptr)
{
    if (ptr)
        cout << *ptr << endl;
}

TEST_CASE("unique_ptr")
{
    UniquePtr<int> up{new int(13)};

    REQUIRE(up.get() != nullptr);

    use(move(up));

    UniquePtr<string> ups{new string("text")};
    cout <<  ups->length() << endl;
}

TEST_CASE("using template classes")
{
    vector<UniquePtr<string>> words;

    words.push_back(UniquePtr<string>(new string("text")));
    words.push_back(UniquePtr<string>(new string("abc")));

    for(auto const& w : words)
        cout << *w << endl;

    SECTION("since C++17")
    {
        vector vec = { 1, 2, 3, 4, 5 };
    }
}
