#include <iostream>
#include <string>
#include <algorithm>
#include <list>
#include <functional>

#include "catch.hpp"

using namespace std;

class Functor
{
public:
    void operator()(string const& msg) const
    {
        cout << "Printing: " << msg << endl;
    }
};

TEST_CASE("functor")
{
    Functor f; // instance of Functor

    f("Hello");
    f("World");
    f("!");
}

class Generator
{
    int seed_;
public:
    Generator(int seed) : seed_{seed}
    {}

    int operator()()
    {
        return ++seed_;
    }
};

TEST_CASE("generating values")
{
    Generator gen{0};

    REQUIRE(gen() == 1);
    REQUIRE(gen() == 2);
    REQUIRE(gen() == 3);

    vector<int> vec(5);

    generate(begin(vec), end(vec), Generator{100});

    REQUIRE(vec == vector<int>{101, 102, 103, 104, 105 });
}


class Lambda_4237654165
{
public:
    int operator()(int x) const { return -x; }
};

TEST_CASE("lambda")
{
    auto negate_value = [](int x) { return -x; };

    REQUIRE(negate_value(1) == -1);

    SECTION("is interpreted as")
    {
        auto negate_value_f = Lambda_4237654165{};

        REQUIRE(negate_value_f(2) == -2);
    }

    SECTION("using std algorithm")
    {
        vector<int> vec = { 1, 2, 3 };

        vector<int> vec_neg(vec.size());

        transform(begin(vec), end(vec), begin(vec_neg), negate_value);

        REQUIRE(vec_neg == vector<int>{-1, -2, -3});
    }
}

///////////////////////////////////////////////////////////
// Callable object
///////////////////////////////////////////////////////////

void foo(int x)
{
    cout << "foo(" << x << ")" << endl;
}

struct Foo
{
    void operator()(int x) const
    {
       cout << "Foo::operator()(" << x << ")" << endl;
    }
};

TEST_CASE("callable")
{
    SECTION("function pointer")
    {
        void (*f)(int) = &foo;

        f(13);
    }

    SECTION("functor")
    {
        Foo f;

        f(13);
    }

    SECTION("lambda")
    {
        auto f = [](int x) { cout  << "lambda(" << x << ")" << endl; };

        f(13);
    }
}

template <typename Container, typename Function>
void my_for_each(Container const& vec, Function f)
{
    for(const auto& item : vec)
        f(item);
}

TEST_CASE("simple my_for_each")
{
    list<int> vec = { 1, 2, 3, 4, 5 };

    my_for_each(vec, &foo);

    cout << "\n";

    my_for_each(vec, Foo{});

    cout << "\n";

    my_for_each(vec, [](int x) { cout << "item(" << x << ")\n"; });
}

///////////////////////////////////////////////////////////////
// std::function
///////////////////////////////////////////////////////////////

TEST_CASE("std::function stores callables")
{
    function<void(int)> f;

    f = &foo;
    f(13);

    f = Foo{};
    f(13);

    f = [](int x) { cout << "lambda(" << x << ")\n"; };
    f(13);
}
