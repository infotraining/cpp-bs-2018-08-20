#include <iostream>
#include <string>
#include <vector>

using namespace std;

int find_at(vector<int> const& vec, size_t index)
{
    throw int{13};

    vector<double> large_vec(10'000'000'000);

    auto& item = vec.at(index);

    cout << "Item[" << index << "] = " << item << endl;

    large_vec[0] = item;
}

int main()
{
    vector<int> vec = {1, 2, 3};

    try
    {
        find_at(vec, 2);
    }
    catch (out_of_range const& e)
    {
        cout << "Catched using out_of_range" << endl;
        cout << e.what() << endl;
    }
    catch (bad_alloc const& e)
    {
        cout << "Catched using bad_alloc" << endl;
        cout << "No available memory" << endl;
        exit(1);
    }
    catch (exception const& e)
    {
        cout << "Catched using exception" << endl;
        cout << e.what() << endl;
    }
    catch (int x)
    {
        cout << "Catched using int: " << x << endl;
    }
    catch (...)
    {
        cout << "Caught - but I dont know what it is" << endl;
    }

    cout << "Normal end of program" << endl;

    string text = "a";


    // not efficient but convinient
    try
    {
        int x = stoi(text);
    }
    catch (invalid_argument const& e)
    {
        cout << "Invalid conversion: " << e.what() << endl;
    }

    // since C++17
//    int x;
//    auto [ptr, err] = from_chars(begin(text), end(text), x);

//    if (err != 0)
//    {

//    }
}
