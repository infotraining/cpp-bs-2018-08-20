#include <string>
#include <iostream>
#include <cstring>
#include <algorithm>
#include <vector>
#include <string_view>

using namespace std;

int main()
{
    // 1st string type - c-string
    const char* ctext = "Hello";

    cout << ctext << endl;

    for(size_t i = 0; i < strlen(ctext); ++i)
        cout << ctext[i] << " ";
    cout << endl;

    const char* alt_ctext = "Hello";

    // ctext & alt_ctext are physically the same array of chars
    const void* raw_mem1 = ctext;
    const void* raw_mem2 = alt_ctext;

    cout << "raw_mem1: " << raw_mem1 << "\n"
         << "raw_mem2: " << raw_mem2 << endl;

    const char* other_ctext = "hello";

    // for comparing strings use strcmp()
    if (strcmp(ctext,other_ctext) < 0)
    {
        cout << ctext << " < " << other_ctext << endl;
    }
    else
    {
        cout << ctext << ">=" << other_ctext << endl;
    }

    // 2nd string type - std::string
    string str_text = "Hello";

    cout << str_text << endl;

    // since C++11 string are compatible with range-based-for
    for(char c : str_text)
        cout << c << " ";
    cout << endl;

    cout << "length of " << str_text << ": " << str_text.length() << endl;

    str_text[0] = 'h';

    cout << str_text << endl;

    string str_alt_text = "Hello";

    if (str_text == str_alt_text)
        cout << str_alt_text << " == " << str_text << endl;
    else
        cout << str_alt_text << " != " << str_text << endl;

    vector<string> words = { "one", "two", "three", "four" };
    sort(begin(words), end(words));

    for(const string& word : words)
        cout << word << " ";
    cout << endl;

    // since C++14
    auto cname = "Jan"; // const char*
    auto str_name = "Hello"s; // string

    cout << cname << " " << str_name << endl;

    // since C++17
    const char* text = "Ala ma kota";
    string_view sv1(text, 3);

    cout << sv1 << endl;

    auto sv_text = "Hello C++"sv;

    // raw-string literals
    const char* path_1 = "c:\\nasz katalog\\twoj katalog\\backup";
    const char* raw_path1 = R"(c:\nasz katalog\twoj katalog\backup)";

    cout << path_1 << endl;
    cout << raw_path1 << endl;

    auto multiline_text = R"(Line1
Line2
Line3)";

    cout << multiline_text << endl;
}
