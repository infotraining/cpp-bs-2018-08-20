#include <iostream>
#include <string>

#include "catch.hpp"

using namespace std;

namespace Drawing
{
    struct Point
    {
        int x, y;
    };

    ostream& operator<<(ostream& out, Point const& pt)
    {
        out << "[" << pt.x << "," << pt.y << "]";
        return out;
    }

    class IShape // abstract class
    {
    public:
        virtual ~IShape()                 = default;
        virtual void move(int dx, int dy) = 0; // pure virtual method
        virtual void draw() const         = 0; // pure virtual method
    };

    class Shape : public IShape // still abstract class - impl of draw is missing
    {
    protected:
        Point coord_;

    public:
        Shape(int x = 0, int y = 0)
            : coord_{x, y}
        {
        }

        void move(int dx, int dy) override
        {
            coord_.x += dx;
            coord_.y += dy;
        }
    };

    class Circle : public Shape
    {
        int radius_;

    public:
        Circle(int x = 0, int y = 0, int r = 0)
            : Shape{x, y}
            , radius_{r}
        {
        }

        void draw() const override
        {
            cout << "Drawing circle at " << coord_ << " with r = " << radius_ << endl;
        }

        int radius() const
        {
            return radius_;
        }

        void set_radius(int radius)
        {
            radius_ = radius;
        }
    };

    class Rectangle : public Shape
    {
        int width_, height_;

    public:
        Rectangle(int x = 0, int y = 0, int w = 0, int h = 0)
            : Shape{x, y}
            , width_{w}
            , height_{h}
        {
        }

        void draw() const override
        {
            cout << "Drawing rectangle at " << coord_
                 << " with width = " << width_
                 << " and with height = " << height_
                 << endl;
        }

        int width() const
        {
            return width_;
        }

        void set_width(int width)
        {
            width_ = width;
        }

        int height() const
        {
            return height_;
        }

        void set_height(int height)
        {
            height_ = height;
        }
    };

    class Line : public Shape
    {
        Point end_;

    public:
        Line(int x1 = 0, int y1 = 0, int x2 = 0, int y2 = 0)
            : Shape{x1, y1}
            , end_{x2, y2}
        {
        }

        void move(int dx, int dy) override
        {
            Shape::move(dx, dy); // calling method from a base class

            end_.x += dx;
            end_.y += dy;
        }

        void draw() const override
        {
            cout << "Drawing a line from " << coord_ << " to " << end_ << endl;
        }
    };
}

using namespace Drawing;

//TEST_CASE("shapes")
//{
//    Shape shp{10, 20};
//    shp.draw();
//    shp.move(1, 1);
//    shp.draw();
//}

TEST_CASE("circle")
{
    Circle c{5, 10, 15};
    c.draw();
    c.move(3, 4);
    c.draw();
}

TEST_CASE("rectangle")
{
    Rectangle rect{1, 2, 10, 20};
    rect.draw();
    rect.move(6, 7);
    rect.draw();
}

TEST_CASE("line")
{
    Line ln{10, 20, 30, 40};
    ln.draw();
    ln.move(9, 15);
    ln.draw();
}

TEST_CASE("polymorhism")
{
    SECTION("upcasting the pointer")
    {
        IShape* ptr_shp = nullptr;

        Circle c{10, 20, 1};
        c.draw();

        ptr_shp = &c;
        ptr_shp->move(5, 10);
        ptr_shp->draw();

        Rectangle rect{1, 2, 10, 20};
        ptr_shp = &rect;
        ptr_shp->draw();

        IShape& ref_shape = rect;
        ref_shape.draw();

        Line ln{10, 20, 30, 40};
        ptr_shp = &ln;
        ptr_shp->draw();
        ptr_shp->move(1, 2);
        ptr_shp->draw();
    }

    SECTION("downcasting")
    {
        IShape* ptr_shp{};

        Circle c{10, 20, 30};

        ptr_shp = &c;

        SECTION("allows to get to methods in derived class")
        {
            SECTION("fast & unsafe")
            {
                Circle* ptr_c = static_cast<Circle*>(ptr_shp);
                ptr_c->set_radius(665);
                ptr_c->draw();
            }

            SECTION("slow & safe")
            {
                SECTION("casting using pointers")
                {

                    SECTION("when cannot cast to pointer returns nullptr")
                    {
                        Line* ptr_l = dynamic_cast<Line*>(ptr_shp);

                        REQUIRE(ptr_l == nullptr);
                    }

                    SECTION("when cast is ok")
                    {
                        Circle* ptr_c = dynamic_cast<Circle*>(ptr_shp);

                        if (ptr_c)
                            ptr_c->set_radius(665);

                        ptr_shp->draw();
                    }
                }

                SECTION("casting using ref")
                {
                    try
                    {
                        Rectangle& ref_rect = dynamic_cast<Rectangle&>(*ptr_shp);
                    }
                    catch(bad_cast const& e)
                    {
                        cout << "Exception caught: " << e.what() << endl;
                    }
                }
            }
        }
    }
}

class Image : public IShape
{
    using ShapePtr = unique_ptr<IShape>;
    vector<ShapePtr> shapes_;
public:
    Image() = default;

    void add(ShapePtr shp)
    {
        shapes_.push_back(std::move(shp));
    }

    void move(int dx, int dy) override
    {
        for(const auto& shp : shapes_)
            shp->move(dx, dy);
    }

    void draw() const override
    {
        cout << "Drawing image:\n";

        for(const auto& shp : shapes_)
            shp->draw();
    }
};

TEST_CASE("Image")
{
    auto img = make_unique<Image>();
    img->add(make_unique<Circle>(10, 20, 30));
    auto eye = make_unique<Circle>(70, 20, 30);
    img->add(move(eye));
    img->add(make_unique<Rectangle>(35, 10, 10, 10));
    img->add(make_unique<Line>(30, 5, 50, 5));

    Image canvas;
    canvas.add(make_unique<Rectangle>(0, 0, 100, 100));
    canvas.add(move(img));

    cout << "\n\n";
    canvas.draw();
}
