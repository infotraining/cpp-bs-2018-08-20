#include <string>
#include <iostream>

using namespace std;



int main()
{
    int x{10};
    string text{"Hello"};
    const double pi = 3.14;
    const string name = "Adam";

    int* ptr_x = nullptr;
    ptr_x = &x;
    cout << *ptr_x << endl;

    int y{665};
    ptr_x = &y;

    int& ref_x = x;
    cout << ref_x << endl;
    ref_x = 13;

    cout << "x: " << x << endl;

    string& ref_text = text;
    ref_text[0] = 'h';
    cout << text << endl;

    double const& ref_pi = pi;
    string const& ref_name = name;
}
