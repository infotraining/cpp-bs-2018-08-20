#include <iostream>
#include <memory>
#include <string>
#include <map>

#include "catch.hpp"

using namespace std;

TEST_CASE("new - delete")
{
    string* heap_str = new string("text");

    cout << *heap_str << endl;

    delete heap_str;
}

TEST_CASE("new[] - delete[]")
{
    string* words = new string[3];

    for (int i = 0; i < 3; ++i)
        words[i] = to_string(i) + "_item"s;

    for (int i = 0; i < 3; ++i)
        cout << words[i] << endl;

    delete[] words;
}

TEST_CASE("Smart Pointers")
{
    SECTION("unique_ptr")
    {

        SECTION("C++11")
        {
            unique_ptr<string> heap_str{new string("text")};

            cout << *heap_str << endl;
            cout << "length: " << heap_str->length() << endl;
        }

        SECTION("C++14")
        {
            auto heap_str = make_unique<string>("text");

            cout << *heap_str << endl;
            cout << "length: " << heap_str->length() << endl;
        }

        SECTION("is move-only type")
        {
            auto heap_str = make_unique<string>("text");

            unique_ptr<string> other_ptr = move(heap_str);

            REQUIRE(heap_str.get() == nullptr);

            if (!heap_str)
            {
                cout << "heap_str is empty" << endl;
                heap_str = make_unique<string>("new text");
            }

            REQUIRE(*other_ptr == "text"s);

            vector<unique_ptr<string>> words;
            words.push_back(make_unique<string>("hello"));
            words.push_back(make_unique<string>("world"));
            words.push_back(move(other_ptr));

            for (const auto& w : words)
                cout << *w << endl;
        }
    }
}


class Gadget
{
    int id_;
public:
    Gadget(int id) : id_{id}
    {
        cout << "Gadget(" << id_ << ")\n";
    }

    ~Gadget()
    {
        cout << "~Gadget(" << id_ << ")\n";
    }

    void use()
    {
        cout << "Using Gadget(" << id_ << ")" << endl;
    }
};

namespace Cpp98
{
    Gadget* create_gadget()
    {
        static int id;
        return new Gadget(++id);
    }

    void use(Gadget* g)
    {
        if (g)
            g->use();
    }

    void use(Gadget& g)
    {
        g.use();
    }

    void use_and_destroy(Gadget* g)
    {
        if (g)
            g->use();

        delete g;
    }

    void buggy_code()
    {
        create_gadget(); // mem-leak

        {
            Gadget* g1 = create_gadget();

            use(g1);
        } // mem_leak

        use(create_gadget()); // mem-leak
        use_and_destroy(create_gadget()); // ok

        {
            Gadget* g1 = create_gadget();
            use_and_destroy(g1);
            g1->use(); // seg-fault
        }
    }
}

namespace ModernCpp
{
    unique_ptr<Gadget> create_gadget()
    {
        static int id;
        return make_unique<Gadget>(++id);
    }

    void use(Gadget* g)
    {
        if (g)
            g->use();
    }

    void use(Gadget& g)
    {
        g.use();
    }

    void use(unique_ptr<Gadget> g)
    {
        if (g)
            g->use();
    }

    void buggy_code()
    {
        create_gadget(); // ok

        {
            unique_ptr<Gadget> g1 = create_gadget();

            use(g1.get());
        } //ok

        use(create_gadget()); // implicit move to function

        {
            unique_ptr<Gadget> g1 = create_gadget();
            use(move(g1));  // visible transfer of ownership
        }
    }
}

TEST_CASE("shared_ptr")
{
    cout << "\n\nusing shared_ptrs:\n";

    map<string, shared_ptr<Gadget>> gadget_pool;

    {
        //shared_ptr<Gadget> local_spg = ModernCpp::create_gadget();
        shared_ptr<Gadget> local_spg = make_shared<Gadget>(1);

        local_spg->use();
        ModernCpp::use(*local_spg);
        ModernCpp::use(local_spg.get());

        auto copy_sp = local_spg;

        REQUIRE(local_spg.use_count() == 2);

        gadget_pool["ipad"] = local_spg;

        REQUIRE(local_spg.use_count() == 3);
    }

    cout << "After scope" << endl;

    REQUIRE(gadget_pool["ipad"].use_count() == 1);

    gadget_pool.clear();

    cout << "End of test" << endl;
}
