#include <string>
#include <iostream>
#include <vector>
#include <numeric>

using namespace std;

double avg(const vector<int>& data)
{
    double sum{}; // the same as: double sum = 0.0;

    for(auto x : data)
        sum += x;

    return sum / data.size();
}

namespace Modern
{
    double avg(const vector<int>& data)
    {
        return accumulate(begin(data), end(data), 0.0) / size(data);
    }
}

void foo(int);

int main()
{
    auto x = 10; // int
    auto ux = 10u; // unsigned int
    auto pi = 3.14; // double
    auto fpi = 3.14f; // float
    auto ctext = "text"; // const char*
    auto stext = "text"s; // string

    auto text = "Hello"s + " " + "world"; // string

    vector<int> data = { 1, 534, 234, 13254 ,215, 33, 55 };

    auto stat_avg = Modern::avg(data);

    cout << "avg: " << stat_avg << endl;

    // conversions
    int ix = 665;
    char cx{x}; // ERROR - narrowing conversion
    cout << (int)cx << endl;
    cout << static_cast<int>(cx) << endl;

    unsigned int uix = 665;
    //int sx2{uix}; // ERROR - narrowing conversion
}
