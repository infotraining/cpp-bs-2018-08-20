#include <algorithm>
#include <array>
#include <cmath>
#include <fstream>
#include <iostream>
#include <list>
#include <numeric>
#include <string>
#include <valarray>
#include <vector>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>

#include "catch.hpp"

using namespace std;

TEST_CASE("pointers")
{
    int x      = 10;
    string str = "text";

    SECTION("can hold address to variable")
    {
        int* ptr_x      = &x;
        string* ptr_str = &str;

        SECTION("allows for indirect access to object")
        {
            *ptr_x = 665;
            REQUIRE(x == 665);

            ptr_str->clear();
            REQUIRE(str == ""s);
        }
    }

    SECTION("can be null")
    {
        SECTION("C++98")
        {
            int* ptr_x = NULL;
        }

        SECTION("since C++11")
        {
            int* ptr_x = nullptr;
            int* ptr_str{};

            REQUIRE(ptr_str == nullptr);
        }
    }

    SECTION("const with pointers")
    {
        SECTION("pointer to constant")
        {
            const double pi = 3.14;

            SECTION("classic declaration")
            {
                const double* ptr_pi = &pi;
                REQUIRE(*ptr_pi == Approx(3.14));
            }

            SECTION("east-const declaration")
            {
                double const* ptr_pi = &pi;
                REQUIRE(*ptr_pi == Approx(3.14));
            }
        }
    }

    int y = 665;

    SECTION("const pointers")
    {
        int* const cptr_x = &x;

        *cptr_x = 13;
        REQUIRE(x == 13);

        // cptr_x = &y; // ERROR - cannot change address in const pointer
    }

    SECTION("const pointer to const")
    {
        const int* const cptr_x = &x;

        // *cptr_x = 665; // ERROR
        // cptr_x = &y;  // ERROR

        SECTION("east const definition")
        {
            int const* const cptr_x = &x;
        }
    }
}

void foo(int x)
{
    cout << "foo(int: " << x << ")\n";
}

using OnErrorHandler = void (*)(string const&);

void report_in_console(string const& msg)
{
    cout << msg << endl;
    exit(1);
}

double divide(int x, int y, OnErrorHandler on_error = &report_in_console)
{
    if (y == 0)
    {
        on_error("Denominator cannot be null");
    }

    return static_cast<double>(x) / y;
}

TEST_CASE("function pointers")
{
    void (*ptr_fun)(int) = nullptr;

    ptr_fun = foo;
    ptr_fun = &foo;

    ptr_fun(10);

    SECTION("can use alias for a type")
    {
        using FunPtr = void (*)(int);

        FunPtr f = &foo;
        f(13);
    }
}

void save_to_file(string const& msg)
{
    ofstream file_out{"log.txt"};

    if (!file_out)
    {
        cout << "File cannot be created" << endl;
    }
    else
    {
        file_out << msg << endl;
    }

    exit(1);
}

TEST_CASE("divide with function as parameter")
{
    int x = 10;
    int y = 1;

    divide(x, y);

    divide(x, y, &save_to_file);
}

void print(int const* tab, size_t size, string const& prefix)
{
    cout << prefix << ": [ ";
    for (int const* it = tab; it != tab + size; ++it)
        cout << *it << " ";
    cout << "]" << endl;
}

template <typename Container>
void print(Container const& c, string const& prefix)
{
    cout << prefix << ": [ ";
    for (const auto& item : c)
        cout << item << " ";
    cout << "]" << endl;
}

int square()
{
    static int x;

    ++x;

    return x * x;
}

TEST_CASE("arrays")
{
    const int size = 10;

    int tab1[size] = {};

    print(tab1, size, "tab1");

    int tab2[10] = {1, 2, 3, 4, 5, 6};

    print(tab2, size, "tab2");
}

TEST_CASE("matrix")
{
    int matrix[3][3] = {};

    for (const auto& row : matrix)
    {
        for (const auto& item : row)
        {
            cout << item << " ";
        }
        cout << endl;
    }
}

TEST_CASE("std::array")
{
    int tab[10] = {};

    SECTION("should replace native array")
    {
        array<int, 10> tab = {1, 2, 3, 4, 5, 6};

        REQUIRE(tab.size() == 10);

        for (auto& item : tab)
            item = -item;

        print(tab.data(), tab.size(), "tab");

        for (auto it = tab.begin(); it != tab.end(); ++it)
            cout << "(" << *it << ") ";
        cout << endl;

        array<int, 10> other = tab;

        print(other, "other");
    }
}

void print_vector_stats(vector<int> const& vec)
{
    cout << "size: " << vec.size() << "; capacity: " << vec.capacity() << endl;
}

TEST_CASE("vector")
{
    SECTION("default construction")
    {
        vector<int> vec;

        REQUIRE(vec.size() == 0);
    }

    SECTION("creating vector with size")
    {
        vector<int> vec(5);

        iota(begin(vec), end(vec), 1);

        print(vec.data(), vec.size(), "vec");

        SECTION("can be resized")
        {
            vec.resize(10);
            print(vec.data(), vec.size(), "vec");
        }
    }

    SECTION("creating vector from list")
    {
        vector<int> vec = {1, 2, 3, 4, 5};

        print(vec.data(), vec.size(), "vec from initializer list");
    }

    SECTION("indexing")
    {
        vector<int> vec = {1, 2, 3, 4, 5};

        vec[4] = 665;

        REQUIRE(vec[4] == 665);

        SECTION("safe at()")
        {
            REQUIRE_THROWS_AS(vec.at(665), std::out_of_range);
        }
    }

    SECTION("iterators")
    {
        vector<int> squares(100);

        //        for(int i = 1; i <= 100; ++i)
        //            squares[i - 1] = i * i;

        //generate_n(begin(squares), squares.size(), square);

        int seed = 0;
        generate_n(begin(squares), squares.size(), [&seed]() { ++seed; return seed * seed; });

        print(squares.data(), squares.size(), "squares");
    }

    SECTION("pushing items to vector")
    {
        cout << "\n---------------------------\n";

        vector<int> vec;
        vec.reserve(32); // important for efficient use of vector

        print_vector_stats(vec);

        for (int i = 0; i < 33; ++i)
        {
            vec.push_back(i);
            print_vector_stats(vec);
        }

        SECTION("shrinking")
        {
            vec.clear();
            print_vector_stats(vec);

            vec.resize(5);
            vec.shrink_to_fit();
            print_vector_stats(vec);
        }
    }
}

TEST_CASE("vector matrix")
{
    vector<vector<int>> matrix = {{1, 2, 3}, {4, 5, 6}, {7, 8, 9}};

    matrix.push_back({1, 1, 1}); // pushing new row
    matrix[1].push_back(7);

    cout << "\n\n";

    for (const auto& row : matrix)
    {
        for (const auto& item : row)
            cout << item << " ";
        cout << endl;
    }

    cout << "\n\n";

    for (int i = 0; i < matrix.size(); ++i)
        for (int j = 0; j < matrix[i].size(); ++j)
            matrix[i][j] = -matrix[i][j];

    for (const auto& row : matrix)
    {
        for (const auto& item : row)
            cout << item << " ";
        cout << endl;
    }
}

TEST_CASE("valarray")
{
    valarray<double> degs(91);
    iota(begin(degs), end(degs), 0);

    print(degs, "degs");

    auto deg_2_rad = [](double deg) { return deg * M_PI / 180.0; };

    cout << "\n\n";

    valarray<double> rads = degs.apply(deg_2_rad);

    print(rads, "rads");

    cout << "\n\n";

    valarray<double> sins = rads.apply(&sin);

    print(sins, "sins");

    cout << "\n\n";

    valarray<double> coss = rads.apply(&cos);

    print(coss, "coss");
}

TEST_CASE("iterators")
{
    list<int> lst = {1, 2, 3, 4, 5};

    SECTION("C++98")
    {
        for (list<int>::iterator it = lst.begin(); it != lst.end(); ++it)
            *it += 1;

        for (list<int>::const_iterator it = lst.begin(); it != lst.end(); ++it)
            cout << *it << " ";
        cout << endl;
    }

    SECTION("C++11")
    {
        for (auto it = lst.begin(); it != lst.end(); ++it)
            *it += 1;

        for (auto it = lst.cbegin(); it != lst.cend(); ++it)
            cout << *it << " ";
        cout << endl;

        SECTION("the same as")
        {
            for(auto& item : lst)
                item += 1;

            for(const auto& item : lst)
                cout << item << " ";
            cout << endl;
        }
    }

    SECTION("in algorithms")
    {
        vector<int> vec = { 2344, 234, 1234, 298, 665, 23423, 2342 };

        auto pos = find(begin(vec), end(vec), 665);

        if (pos != end(vec))
            cout << "Found: " << *pos << endl;
        else
            cout << "Item not found" << endl;

        auto is_even = [](int x) { return x % 2 == 0; };

        REQUIRE_FALSE(is_even(7));
        REQUIRE(is_even(2));

        auto evens_end = partition(begin(vec), end(vec), is_even);

        cout << "Evens: ";
        for(auto it = begin(vec); it != evens_end; ++it)
            cout << *it << " ";
        cout << endl;

        cout << "Odds: ";
        for(auto it = evens_end; it != end(vec); ++it)
            cout << *it << " ";
        cout << endl;
    }
}

TEST_CASE("set")
{
    cout << "\n\nset:\n";

    set<int> mset = { 5, 23, 346, 32, 1, 2345, 9 };

    print(mset, "mset");

    mset.insert(666);
    mset.insert(666);
    mset.insert(666);
    mset.insert(665);
    mset.erase(32);

    auto pos = mset.find(23);

    if (pos != end(mset))
        cout << *pos  << " is inside mset" << endl;

    print(mset, "mset");
}

TEST_CASE("map")
{
    cout << "\n\nmap:\n";

    map<int, string> dict = { {1, "one"}, {3, "three"}, {4, "four"} };

    REQUIRE(dict[1] == "one"s);

    dict[0];

    REQUIRE(dict.size() == 4);
    REQUIRE(dict[0] == ""s);

    REQUIRE_THROWS_AS(dict.at(2), out_of_range);
    REQUIRE(dict.find(2) == end(dict));

    dict[0] = "zero"s;
    dict.insert(make_pair(2, "two"));

    for(const auto& kv : dict)
        cout << kv.first << " - " << kv.second << endl;
}

TEST_CASE("unordered_set")
{
    cout << "\n\nunordered_set:\n";

    unordered_set<int> uset = { 5, 23, 346, 32, 1, 2345, 9 };

    print(uset, "mset");

    uset.insert(666);
    uset.insert(666);
    uset.insert(666);
    uset.insert(665);
    uset.erase(32);

    auto pos = uset.find(23);

    if (pos != end(uset))
        cout << *pos  << " is inside mset" << endl;

    print(uset, "mset");
}

TEST_CASE("unordered_map")
{
    cout << "\n\nunordered_map:\n";

    unordered_map<int, string> dict = { {1, "one"}, {3, "three"}, {4, "four"} };

    REQUIRE(dict[1] == "one"s);

    dict[0];

    REQUIRE(dict.size() == 4);
    REQUIRE(dict[0] == ""s);

    REQUIRE_THROWS_AS(dict.at(2), out_of_range);
    REQUIRE(dict.find(2) == end(dict));

    dict[0] = "zero"s;
    dict.insert(make_pair(2, "two"));

    for(const auto& kv : dict)
        cout << kv.first << " - " << kv.second << endl;
}
